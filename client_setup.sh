#!/bin/bash
#                         __
#                      _,'  `:--.__
#                    ,'    .       `'--._
#                  ,'    .               `-._
#          ,-'''--/    .                     `.
#        ,'      /   .    ,,-''`-._            `.
#       ; .     /   .   ,'         `,            |____
#       |      /   .   ;             :           |--._\
#       '     /   :    |      .      |           |
#        `.  ;   _     :             ;           |
#          `-:   `"     \           ,           _|==:--.__
#             \.-------._`.       ,`        _.-'     `-._ `'-._
#              \  :        `-...,-``-.    .'             `-.   | 
#               `.._         / | \     _.'                  `. | 
#                   `.._    '--'```  .'                       `|
#                       `.          /
#                .        `-.       \
#         ___   / \  __.--`/ , _,    \
#       ,',  `./,--`'---._/ = / \,    \  __
#      /    .-`           `"-/   \)_    "`
#    _.--`-<_         ,..._ /,-'` /    /
#  ,'.-.     `.    ,-'     `.    /`'.+(
# / /  /  __   . ,'    ,   `.  '    \ \ 
# |(_.'  /  \   ; |          |        ""_
# |     (   ;   `  \        /           `.
# '.     `-`   `    `.___,-`             `.
#   `.        `                           |
#    ; `-.__`                             |
#    \    -._                             |
#     `.                                  /
#      /`._                              /
#      \   `,                           /
#       `---'.     /                  ,'
#             '._,'-.              _,(_,_
#                    |`--.    ,,.-' `-.__)
#                     `--`._.'         `._)
#                                         `=-
# ____  ___ ____  _____   _____ _   _ _____   ____ ___ ____ _ 
#|  _ \|_ _|  _ \| ____| |_   _| | | | ____| |  _ \_ _/ ___| |
#| |_) || || | | |  _|     | | | |_| |  _|   | |_) | | |  _| |
#|  _ < | || |_| | |___    | | |  _  | |___  |  __/| | |_| |_|
#|_| \_\___|____/|_____|   |_| |_| |_|_____| |_|  |___\____(_)
#
# A script by Patrick Veronneau
#
# Version 1.0
#
#
# Side note: Nicole Matsui is a jerk for making me comment.
#
#### Client setup for raspberry pi to serve RTMP stream
# Verify this is run by root
if [ "$(id -u)" != "0" ]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi
echo "Enter Server hostname or IP"
read SERVERNAME
echo "Enter Stream key"
READ STREAMKEY
echo "** Installing packages **"
apt-get update
apt-get install rtmpdump omxplayer
echo "** Adding Socket **"
mkfifo /run/rtmp_socket
chown pi:pi /run/rtmp_socket
cat << EOF > /etc/tmpfiles.d/rtmp_socket.conf
p+ /run/rtmp_socket 0755 pi pi -
EOF
echo "** Installing systemd services **"
cat << EOF > /lib/systemd/system/rtmpdump.service
 [Unit]
 Description=RTMP Dump to grab live video stream
 After=network.target

 [Service]
 ExecStart=/usr/bin/rtmpdump -r rtmp://$SERVERNAME/live/$STREAMKEY --live -o /run/rtmp_socket
 Restart=always
 RestartSec=10
 User=pi

 [Install]
 WantedBy=multi-user.target
EOF
cat << EOF > /lib/systemd/system/omxplayer.service
 [Unit]
 Description=Video Player for RTMP Stream
 After=rtmpdump.service

 [Service]
 ExecStart=/usr/bin/omxplayer --live --display 0.0 -o hdmi /run/rtmp_socket
 Restart=always
 RestartSec=10
 User=pi

 [Install]
 WantedBy=multi-user.target
EOF
echo "** Adding restart script to /home/pi/restart_stream.sh **"
cat << EOF > ~pi/restart_stream.sh
sudo systemctl restart rtmpdump.service
sudo systemctl restart omxplayer.service
EOF
chmod 755 ~pi/restart_stream.sh
echo "** Enabling and starting services **"
systemctl daemon-reload
systemctl enable rtmpdump.service && systemctl start rtmpdump.service
systemctl enable omxplayer.service && systemctl start omxplayer.service
