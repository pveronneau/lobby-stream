# OBS Stream Server
An Open Broadcast Software (OBS) Studio RTMP Server and Connected Raspberry Pi client.
![alt text][Layout]

## Requirements
* An Ubuntu or Windows 10 system to run OBS Studio and docker
* A raspberry pi running raspbian

## Server Setup

### Ubuntu
As root, execute `server_setup.sh`. This will download the necessary packages, setup docker and start an NGINX RTMP proxy on port 1935.

### Windows 10
The windows 10 setup is a bit more complex.  Under an administrative power shell terminal, execute `server_setup-win.ps1`.  If you have not yet setup the linux execution environment a reboot will be required.  After the reboot run the script again to complete the docker setup.  Just like the ubuntu setup, a docker based NGINX RTMP proxy should be running on port 1935.

### OBS Studio Configuration
In OBS Studio click on the settings button, then under stream, select custom.  The server should be `rtmp://127.0.0.1/live` and select a stream key.
![alt text][OBSConfig] 


In this example I selected a stream key of `lobby`.

### Add the cameras
Full details on adding an RTSP camera can be found here https://selimatmaca.com/index.php/live-streaming/138-encoder-software


Once that is complete import an image or test video and click start streaming.  

#### Testing the Server
You can verify the stream using [VLC media player]([https://www.videolan.org/vlc/index.html]) and connecting directly to the stream.
![alt text][vlctest]

## Client Setup
For the client setup run `client_setup.sh` and enter the hostname or external ip of your server, and the streaming key.


The script will setup two systemd services, rtmpdump.service and omxplayer.service.

### Restarting the client
The script creates a restart script located in the pi users home directory.  Execute `~pi/restart_stream.sh` to restart both the socket provder and the video player.


[Layout]: https://bitbucket.org/pveronneau/lobby-stream/raw/master/images/OBS_stream_layout.PNG "Layout"
[OBSConfig]: https://bitbucket.org/pveronneau/lobby-stream/raw/master/images/OBS_stream_config.PNG "OBS config"
[vlctest]: https://bitbucket.org/pveronneau/lobby-stream/raw/master/images/vlc_test.PNG "VLC Test"